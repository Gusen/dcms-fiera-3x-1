
/.htaccess:755
/forum/files/:755

/files/:755
/files/avatars/:755
/files/avatars_list/:755
/files/foto/:755
/foto/:755

/style/themes/:755

/sys/obmen/files/:755
/sys/obmen/screens/:755

/sys/ini/:755

/sys/cache/:755
/sys/cache/users/:755
/sys/cache/other/:755
/sys/cache/images/:755

/sys/tmp/:755

/sys/sql_update/:755
/sys/update_fiera/:755
/sys/update_user/:755


/sys/modules/files/:755
/sys/modules/log/:755
/sys/modules/backup/:755
/sys/modules/tmp/:755

/sys/dat/:755
/sys/dat/default.ini:755
/sys/dat/if_password.txt:755
/sys/dat/if_reg.txt:755
/sys/dat/if_user_link_dir.txt:755
/sys/dat/link_dir_err.txt:755
/sys/dat/if_shell_dir.txt:755
/sys/dat/chmod.txt:755

/sys/dat/settings.conf:755
/sys/dat/shif.conf:755


